<div align="center">
  <h1>当Monorepo 遇上uniapp! 在vben3的项目参考下，将uniapp融入其中</h1>
</div>
<div align="center">

[English](./README.en-US.md) | 简体中文

## 简介

Monorepo-uniapp 是一个免费开源的uniapp模版。使用了最新的`vue3`,`vite4`,`TypeScript`等主流技术开发，开箱即用的多端开发解决方案，也可用于学习参考。

- 项目仓库地址：[https://gitee.com/ggbhack/monorepo-uniapp](https://gitee.com/ggbhack/monorepo-uniapp)
- 项目联系人微信：15285072859

## 安装

```
git clone https://gitee.com/ggbhack/monorepo-uniapp.git
pnpm i
```

具体运行，请到apps/sk-mini中查看
