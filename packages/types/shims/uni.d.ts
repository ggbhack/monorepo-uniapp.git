declare module 'uview-plus' {
  export function install(): void

  interface test {
    /** 邮箱格式校验 */
    email(email: string): boolean
  }

  global {
    interface Uni {
      $u: any
    }
  }
}
