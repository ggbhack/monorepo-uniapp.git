/**
 * @link https://prazdevs.github.io/pinia-plugin-persistedstate/zh/guide/
 */

import { PersistedStateFactoryOptions } from 'pinia-plugin-persistedstate'

export function persistGlobalConfig(
  keyPrefix: string,
): PersistedStateFactoryOptions {
  return {
    // storage: localStorage,
    storage: {
      getItem(key: string) {
        return uni.getStorageSync(key)
      },
      setItem(key: string, value: any) {
        uni.setStorageSync(key, value)
      },
    },
    key: (id) => `${keyPrefix}__${id}`,
  }
}
