import type { App } from 'vue'
import type { I18nOptions } from 'vue-i18n'
import { localeSetting } from './config'
import { createI18n } from 'vue-i18n'
import { setHtmlPageLang, setLoadLocalePool } from './helper'
import { getLocale } from './store'
import zh_CN from './lang/zh_CN'
import en from './lang/en'

const { fallback, availableLocales } = localeSetting

export let i18n: ReturnType<typeof createI18n>

const createI18nOptions = async (): Promise<I18nOptions> => {
  const locale = getLocale.value

  // 这里改用另外一种方式
  // const defaultLocal = await import(`./lang/${locale}.ts`)
  // const message = defaultLocal.default?.message ?? {}
  const messages: Record<string, any> = {
    en,
    zh_CN,
  }
  const message = messages[locale].message ?? {}

  setHtmlPageLang(locale)

  setLoadLocalePool((loadLocalePool) => loadLocalePool.push(locale))

  return {
    legacy: true,
    locale,
    fallbackLocale: fallback,
    messages: {
      [locale]: message,
    },
    availableLocales: availableLocales,
    sync: true,
    silentTranslationWarn: true,
    missingWarn: false,
    silentFallbackWarn: true,
  }
}

// setup i18n instance with glob
export const setupI18n = async (app: App) => {
  const options = await createI18nOptions()
  i18n = createI18n(options)
  app.use(i18n)
}
