import { genMessage } from '../helper'

const modules = import.meta.glob('./en/**/*.ts', { eager: true }) as any

export default {
  message: genMessage(modules, 'en'),
}
