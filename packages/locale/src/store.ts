import type { LocaleType } from '@vben/types'
import { computed } from 'vue'
import { LOCALE_KEY } from '@vben/constants'
import { useLocalStorage } from '@vben/utils'
import { localeSetting } from './config'

const store = useLocalStorage(LOCALE_KEY, localeSetting)

export function setLocale(locale: LocaleType) {
  uni.setStorageSync(LOCALE_KEY, {
    ...localeSetting,
    locale,
  })
  store.value.locale = locale
}

export const getLocale = computed(() => {
  const local: any = uni.getStorageSync(LOCALE_KEY)
  return local?.locale || store.value.locale
})
