// import { defineConfig } from "vite";
// import uni from "@dcloudio/vite-plugin-uni";

// // https://vitejs.dev/config/
// export default defineConfig({
//   plugins: [uni()],
// });

import { createViteConfig } from '@config/vite'
import { defineConfig, UserConfig } from 'vite'

export default defineConfig(async ({ command, mode }): Promise<UserConfig> => {
  return await createViteConfig(command, mode, process.cwd())
})
