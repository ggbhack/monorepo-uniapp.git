import { request } from '@vben/request'

enum Api {
  GET_POST = '/posts',
}

export const getPosts = () => request.get<void>({ url: Api.GET_POST })
