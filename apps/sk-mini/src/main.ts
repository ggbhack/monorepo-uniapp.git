import { createSSRApp } from 'vue'
import '@vben/styles'

import { setupPinia } from '@vben/stores'
import { initApplication } from './init-application'
import mpShare from '@/uni_modules/uview-plus/libs/mixin/mpShare'

import App from './App.vue'
import { registerGlobComp } from './components/registerGlobComp'
import 'virtual:uno.css'

export function createApp() {
  const app = createSSRApp(App)
  // 初始化pinia
  setupPinia(app)
  // 初始化应用程序
  initApplication(app)
  // 全局组件注册
  registerGlobComp(app)
  // 配置分享
  // #ifdef MP
  app.mixin(mpShare)
  // #endif
  return {
    app,
  }
}
