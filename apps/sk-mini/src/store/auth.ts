import { defineStore } from '@vben/stores'

// import { useUserStore } from './user'
// import { getPermCode } from '@/apis/auth'
// import { toRaw, unref } from 'vue'
// import { PermissionModeEnum, PageEnum } from '@vben/constants'
// import { useAppConfig } from '@vben/hooks'

interface AuthState {
  // Permission code list
  permCodeList: string[] | number[]
}

export const useAuthStore = defineStore('app-auth-store', {
  state: (): AuthState => ({
    permCodeList: [],
  }),
  getters: {
    getPermCodeList(): string[] | number[] {
      return this.permCodeList
    },
  },
  actions: {
    setPermCodeList(codeList: string[]) {
      this.permCodeList = codeList
    },
  },
})
// Need to be used outside the setup
export function useAuthStoreWithout() {
  return useAuthStore()
}
