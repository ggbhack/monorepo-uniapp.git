import { defineStore } from '@vben/stores'
import { UserInfo, RoleInfo, UserState } from '@vben/types'

export const useUserStore = defineStore('app-user-store', {
  persist: {
    paths: ['userInfo', 'accessToken', 'roles'],
  },
  state: (): UserState => ({
    userInfo: null,
    accessToken: undefined,
    roles: [],
    // Whether the login expired
    sessionTimeout: false,
    // Last fetch time
    lastUpdateTime: 0,
    count: 0,
  }),
  getters: {
    getUserInfo(): UserInfo | null {
      return this.userInfo
    },
    getAccessToken(): string | undefined {
      return this.accessToken
    },
    getRoles(): RoleInfo[] {
      return this.roles.length > 0 ? this.roles : []
    },
    getSessionTimeout(): boolean {
      return !!this.sessionTimeout
    },
    getLastUpdateTime(): number {
      return this.lastUpdateTime
    },
  },
  actions: {
    setAccessToken(info: string | undefined) {
      this.accessToken = info ? info : ''
    },
    setRoles(roles: RoleInfo[]) {
      this.roles = roles
    },
    setUserInfo(info: UserInfo | null) {
      this.userInfo = info
      this.lastUpdateTime = new Date().getTime()
    },
    setSessionTimeout(flag: boolean) {
      this.sessionTimeout = flag
    },
    resetState() {
      this.userInfo = null
      this.accessToken = undefined
      this.roles = []
      this.sessionTimeout = false
    },
    // 测试专用
    synIncrease() {
      this.count += 1
    },
    async asyncIncrease() {
      await new Promise((resolve) => setTimeout(resolve, 1000))
      this.count += 1
    },
  },
})

// Need to be used outside the setup
export function useUserStoreWithout() {
  return useUserStore()
}
