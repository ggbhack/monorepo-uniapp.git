# Monorepo uniapp

uni-app Vue3 + TypeScript + Vite + Pinia + Unocss 模板项目

## 项目架构搭建中

---

- [x] vite-config
- [x] ts-config
- [x] unocss 样式
- [x] pinia 状态管理
- [x] env 环境变量
- [x] uview-plus ui库
- [x] mescroll-uni 刷新组件
- [x] lint-staged 提交前校验
- [x] pinia 缓存   

---

## 以下遇到的问题汇总

[为什么 uni-app (vue3) 和 @vueuse/core v10 一起用会报错？](https://juejin.cn/post/7308899425149042739)

```js
function createMockComponent(name) {
  return {
    setup() {
      throw new Error(`[vue-demi] ${name} is not support vue2`)
    },
  }
}

export var TransitionGroup = /*__PURE__*/ createMockComponent('TransitionGroup')
```

[Error: module 'common/side-channel.js' is not defined, require args is 'side-channel'](https://ask.dcloud.net.cn/question/177458)
qs版本问题，换成6.5.3或其他版本试试

[uniapp 封装axios adapter 运行到小程序报错 Module not found: Error: Package path ./lib/core/settle is not exported from package D:\xxx\xxx\n...](https://www.jianshu.com/p/d1d6e0751d97)
跟换axios的版本为 "axios": "^0.26.1"

多语言打包小程序无效
换一种方式引入 注意 use-locale.ts也需要 处理

```js
import zh_CN from './lang/zh_CN'
import en from './lang/en'
// 这里改用另外一种方式
  // const defaultLocal = await import(`./lang/${locale}.ts`);
  // const message = defaultLocal.default?.message ?? {};
  const messages: Record<string, any> = {
    en,
    zh_CN
  }
  // // const defaultLocal = zh_CN;
  // const message = defaultLocal ?? {};
  const message = messages[locale].message ?? {}
```

改造 getAppConfig

```js
// 打包微信小程序报错 __PRODUCTION__VBEN_ADMIN__ 报错无法读取
function getAppConfig(env: Record<string, any>) {
  // const ENV_NAME = getAppConfigFileName(env)

  // const ENV = (
  //   env.DEV
  //     ? // Get the global configuration (the configuration will be extracted independently when packaging)
  //       env
  //     : window[ENV_NAME]
  // ) as GlobEnvConfig
  const ENV = env as GlobEnvConfig

  // console.log(env,env.DEV)
  const { VITE_GLOB_APP_SHORT_NAME } = ENV

  if (!/^[a-zA-Z\_]*$/.test(VITE_GLOB_APP_SHORT_NAME)) {
    console.warn(
      `VITE_GLOB_APP_SHORT_NAME Variables can only be characters/underscores, please modify in the environment variables and re-running.`,
    )
  }

  return ENV
}
```

unocss 打包小程序 样式报错多一个 \
固定unocss的版本号 - 后续看能不能解决控制台的警告

```json
{
  "unocss": "^0.51.8",
  "unocss-preset-weapp": "^0.2.1"
}
```

[uniapp 的缓存处理 参考](https://blog.csdn.net/weixin_43191327/article/details/134071863)

支持小程序，H5，App

![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzIxODc4OTk1OQ==653218789959)

![Unocss](https://fastly.jsdelivr.net/gh/MellowCo/image-host/2022/202211121156442.png)

|                                     H5                                      |                                 微信小程序                                  |                                  App(iOS)                                   |                                        App(Android)                                         |
| :-------------------------------------------------------------------------: | :-------------------------------------------------------------------------: | :-------------------------------------------------------------------------: | :-----------------------------------------------------------------------------------------: |
| ![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzE5Mzc4MzUyMQ==653193783521) | ![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzE5Mzc1Mzk1MQ==653193753951) | ![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzIxMDc2NTcwNg==653210765706) | <img src="https://img.cdn.sugarat.top/mdImg/MTY1MzIxMzkyOTQxNg==653213929416" width="360"/> |

**Node >= 17.**

**pnpm 7**

## 使用

### 安装依赖

**建议使用 pnpm，依赖安装速度更快**

```sh
npm i -g pnpm
```

```sh
pnpm install
```

**MAC M1(ARM 芯片)，其它操作系统无需关注**，正常运行需要手动安装 `esbuild-darwin-64`即可

```sh
pnpm add esbuild-darwin-64@0.15.13 -D
```

## 本地启动

### 微信小程序

```sh
# 构建出产物
pnpm dev:mp-weixin
```

> **Q1：** 如果 dev 的时候发现报错，可以尝试删除`node_modules`之后再在命令行中运行`pnpm install --shamefully-hoist`重新安装依赖再`pnpm dev:mp-weixin`
>
> [详细参考文档](https://pnpm.io/zh/faq#%E8%A7%A3%E5%86%B3%E6%96%B9%E6%A1%883)

> **Q2：** 如果运行白屏，有报错信息 “app.js 错误 ReferenceError: regeneratorRuntime is not defined”
>
> 参考[解决方案](https://blog.csdn.net/FUFCY/article/details/125160828) 给微信小程序 IDE 开启**增强编译选项**

然后将编译结果`dist/dev/mp-weixin`导入微信开发者工具即可运行

<details>
<summary>点击查看 导入详细步骤</summary>

![图片](https://img.cdn.sugarat.top/mdImg/MTYzNzQxNjc3MjA4Mw==637416772083)

![图片](https://img.cdn.sugarat.top/mdImg/MTYzNzQxNjg4MTUwNA==637416881504)

![图片](https://img.cdn.sugarat.top/mdImg/MTYzNzQxNjY3OTY0NQ==637416679645)

</details>

### H5

```sh
# CSR
pnpm dev:h5
# SSR
pnpm dev:h5:ssr
```

根据提示，打开对应地址即可访问

![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzIxMTE0MDEzMg==653211140132)

### App

> **Q1：** 如启动到 App 侧有报错？
> 请更新至最新的 HBuilderX-Alpha 客户端

#### 安装一些必要工具

需要使用 `uni-app` 官方提供的 [HBuilderX](https://www.dcloud.io/hbuilderx.html) 启动项目

**Android 模拟器在 MacOSX、Windows 上都可以安装；iOS 模拟器只能在 MacOSX 上安装。**

先安装相关模拟器，[详细参考文档](https://hx.dcloud.net.cn/Tutorial/App/installSimulator)

- 安卓：[夜神模拟器](https://www.yeshen.com/blog/)
- iOS：Mac 上安装 Xcode

准备就绪后，使用 HBuilderX 打开项目

#### iOS 模拟器运行

通过顶部菜单栏，找到运行入口

![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzIxMjk1MTgzNw==653212951837)

选择一个目标设备，点击启动即可

![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzIxMjk3NDM0NQ==653212974345)

#### Android 模拟器运行

这里以[夜神模拟器](https://www.yeshen.com/blog/)为例

<details>
<summary>点击查看 详细步骤</summary>

先通过 HBuilderX 修改模拟器端口为 `62001`

![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzIxNDAzMjIwNg==653214032206)

打开夜神模拟器

![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzIxNDA5OTYxNg==653214099616)

选择运行到 Android 基座

![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzIxNDEzMzI0OA==653214133248)

选择已经打开的模拟器，点击运行即可
![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzIxNDIxNjczNw==653214216737)

![图片](https://img.cdn.sugarat.top/mdImg/MTY1MzIxMzkyOTQxNg==653213929416)

</details>

## 打包构建

### 微信小程序

```
pnpm build:mp-weixin
```

### H5

```sh
# CSR
pnpm build:h5
# SSR
pnpm build:h5:ssr
```

### App

基于 `HBuilderX` 参考[官方文档](https://hx.dcloud.net.cn/Tutorial/App/SafePack)进行进一步的操作

其它更多运行脚本 查看 [package.json](./package.json)中的 scripts
