import { dirResolver } from 'vite-auto-import-resolvers'
import type { Resolver } from 'unplugin-auto-import/types'
import AutoImport from 'unplugin-auto-import/vite'
import { PluginOption } from 'vite'

type Arrayable<T> = T | Array<T>
type Resolvers = Arrayable<Arrayable<Resolver>>

export function AutoImportPlugin() {
  const plugins: PluginOption[] = []
  const AutoImportResolvers: Resolvers = []
  AutoImportResolvers.push(
    dirResolver({ prefix: 'use' }),
    dirResolver({
      target: 'stores',
      suffix: 'Store',
    }),
  )
  plugins.push(
    AutoImport({
      dts: '@vben/types/shims/auto-import.d.ts',
      imports: ['vue', 'pinia', 'vue-i18n', '@vueuse/core'],
      resolvers: AutoImportResolvers,
    }),
  )
  return plugins
}
