import type { PluginOption } from 'vite'
import type { ViteEnv } from '../utils'
// import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import legacy from '@vitejs/plugin-legacy'
import purgeIcons from 'vite-plugin-purge-icons'
// import { configHtmlPlugin } from './html'
import { configMockPlugin } from './mock'
import { configCompressPlugin } from './compress'
import { configVisualizerConfig } from './visualizer'
import { configImageminPlugin } from './imagemin'
import { configSvgIconsPlugin } from './svg-icons'
import { configUnocssPlugin } from './unocss'
// import { createConfigPlugin } from './config'
import { configHttpsPlugin } from './https'
import { AutoImportPlugin } from './auto-import'
import MonoRepoAliasPlugin from './monorepo'
import uni from '@dcloudio/vite-plugin-uni'

export async function configVitePlugins(
  root: string,
  viteEnv: ViteEnv,
  isBuild: boolean,
) {
  const {
    VITE_USE_IMAGEMIN,
    VITE_USE_MOCK,
    VITE_LEGACY,
    VITE_BUILD_COMPRESS,
    VITE_BUILD_COMPRESS_DELETE_ORIGIN_FILE,
  } = viteEnv

  const vitePlugins: (PluginOption | PluginOption[])[] = [
    // handle .vue files
    // vue(),
    // have to
    vueJsx(),
    // uniapp
    uni(),
  ]

  // api 自动按需引入
  vitePlugins.push(AutoImportPlugin())
  // @vitejs/plugin-legacy
  VITE_LEGACY && isBuild && vitePlugins.push(legacy())

  // vite-plugin-html
  // #ifdef H5
  // vitePlugins.push(await configHtmlPlugin(root, viteEnv, isBuild))
  // #endif

  // unocss
  vitePlugins.push(configUnocssPlugin())

  // #ifdef H5
  // vitePlugins.push(createConfigPlugin())
  // #endif

  // vite-plugin-svg-icons
  vitePlugins.push(configSvgIconsPlugin(isBuild))

  // vite-plugin-mock
  VITE_USE_MOCK && vitePlugins.push(configMockPlugin(isBuild))

  // vite-plugin-purge-icons
  vitePlugins.push(purgeIcons())

  // rollup-plugin-visualizer
  vitePlugins.push(configVisualizerConfig())

  // http2
  vitePlugins.push(configHttpsPlugin(viteEnv))

  // MonorepoSupport
  vitePlugins.push(MonoRepoAliasPlugin())

  // The following plugins only work in the production environment
  if (isBuild) {
    // vite-plugin-imagemin
    VITE_USE_IMAGEMIN && vitePlugins.push(configImageminPlugin())

    // rollup-plugin-gzip
    vitePlugins.push(
      configCompressPlugin(
        VITE_BUILD_COMPRESS,
        VITE_BUILD_COMPRESS_DELETE_ORIGIN_FILE,
      ),
    )
  }

  return vitePlugins
}
