/**
 * The instant on-demand atomic CSS engine.
 * @see https://github.com/unocss/unocss
 */

import Unocss from 'unocss/vite'
import { presetIcons, presetMini, transformerDirectives } from 'unocss'
import {
  transformerAttributify,
  transformerClass,
} from 'unocss-preset-weapp/transformer'
import presetWeapp from 'unocss-preset-weapp'

export function configUnocssPlugin() {
  return Unocss({
    content: { pipeline: { exclude: ['node_modules', '.git', 'dist'] } },
    presets: [
      presetIcons(),
      presetMini({ dark: 'class' }),
      presetWeapp() as any,
      // https://github.com/MellowCo/unocss-preset-weapp/tree/main/src/transformer/transformerAttributify
      transformerAttributify() as any,
    ],
    shortcuts: {
      'flex-center': 'flex justify-center items-center',
      'grid-center': 'grid place-content-center',
    },
    theme: {
      colors: {
        primary: 'var(--primary-color)',
      },
      backgroundColor: {},
      transitionProperty: [],
    },
    transformers: [
      transformerDirectives({
        enforce: 'pre',
      }),
      // // https://github.com/MellowCo/unocss-preset-weapp/tree/main/src/transformer/transformerAttributify
      // // transformerAttributify(),
      // // https://github.com/MellowCo/unocss-preset-weapp/tree/main/src/transformer/transformerClass
      transformerClass() as any,
    ],
  })
}
